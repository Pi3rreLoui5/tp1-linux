# B2-TP1 Linux
 
##  0 Préparation de la machine

 - **Accès internet**

Carte réseau dédiée :

**cmd :** ```ip a```

![](https://i.imgur.com/oPmhI1d.png)

La carte réseau dédiée est la enp0s3.

Route par défaut :

**cmd :** ```ip r s```

![](https://i.imgur.com/g87EbX4.png)

La route par défaut est bien la enp0s3.

- **Un accès à un réseau local**

*Carte réseau dédiée :* 

On veut accéder au fichier correspondant à l'interface et on répéte l'opération sur les 2 machines.

```sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8```

on met respectivement les informations suivantes : 

```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11                   
NETMASK=255.255.255.0
DNS=1.1.1.1
```
et 10.101.1.12 sur la 2eme VM.

On répéte l'opération sur les 2 machines et on ping.

- **Vérification de dig**

**node1**

```
[pilou@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 2477
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9810    IN      A       92.243.16.143

;; Query time: 119 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Tue Sep 28 15:03:53 CEST 2021
;; MSG SIZE  rcvd: 53
```
L'IP qui correspont au nom demandé est ```ynov.com.               9810    IN      A       92.243.16.143```

L'IP du serveur qui vous a répondu ```;; SERVER: 192.168.1.1#53(192.168.1.1)```

**node2**
```
[pilou@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 293 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Tue Sep 28 14:50:06 CEST 2021
;; MSG SIZE  rcvd: 53
```
L'IP qui correspont au nom demandé est ```ynov.com.               10800   IN      A       92.243.16.143```

L'IP du serveur qui vous a répondu ```;; SERVER: 192.168.1.1#53(192.168.1.1)```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**

**node1**
```
[pilou@node1 ~]$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.565 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.478 ms
64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.531 ms
^C
--- node2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2072ms
rtt min/avg/max/mdev = 0.478/0.524/0.565/0.044 ms
```

**node2**
```
[pilou@node2 ~]$ ping node1
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.348 ms
64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.488 ms
64 bytes from node1 (10.101.1.11): icmp_seq=3 ttl=64 time=0.408 ms
64 bytes from node1 (10.101.1.11): icmp_seq=4 ttl=64 time=0.537 ms
^C
--- node1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3102ms
rtt min/avg/max/mdev = 0.348/0.445/0.537/0.074 ms
```

- **Firewall**

**node1**

```
[pilou@node1 ~]$ sudo firewall-cmd --remove-service cockpit
success
[pilou@node1 ~]$ sudo firewall-cmd --remove-service dhcpv6-client
success
[pilou@node1 ~]$ sudo firewall-cmd --add-port 22/tcp
success
[pilou@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**node2**

```
[pilou@node2 ~]$ sudo firewall-cmd --remove-service cockpit
success
[pilou@node2 ~]$ sudo firewall-cmd --remove-service dhcpv6-client
success
[pilou@node2 ~]$ sudo firewall-cmd --add-port 22/tcp
success
[pilou@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

```
[pilou@node1 home]$ sudo useradd pilon -m -s /bin/bash -u 100
[pilou@node1 home]$ ls
pilon  pilou

```
**Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.**

```
[pilou@node1 /]$ sudo groupadd admins
[sudo] password for pilou:
[pilou@node1 /]$ sudo visudo
```
Et j'ai marqué ```%admins ALL=(ALL) ALL```


**Ajouter votre utilisateur à ce groupe admins**

```
[pilou@node1 /]$ sudo usermod -aG admins pilon
[sudo] password for pilou:
[pilou@node1 /]$ groups pilon
pilon : pilon admins
```

### 2. SSH

```
PS C:\Users\berti> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\berti/.ssh/id_rsa): C:\Users\berti/.ssh/id_keyssh
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\berti/.ssh/id_keyssh.
Your public key has been saved in C:\Users\berti/.ssh/id_keyssh.pub.
The key fingerprint is:
SHA256:Is6Y2mMGL3lgVv240iOMjU7AH78r6f1VxwkSXBP8+mw berti@Asus-Pilou
The key's randomart image is:
+---[RSA 4096]----+
|        ..o+.    |
|         ....    |
|    .    . ..    |
|.  . .    . o..  |
|..... + S  ..+   |
|+o.=oo o  ...    |
|o*B.=..  .  o    |
|+=*B.+. .    E   |
|o*oo+++.    .    |
+----[SHA256]-----+
```
J'ai du mal configurer le ssh parce qu'il me demande quand meme toujours le mot de passe et la commande ```ssh-copy-id``` ne fonctionne pas sur mon ordinateur (windows)
```
[pilou@node1 ~]$ sudo nano /.ssh/authorized_keys
[sudo] password for pilou:
[pilou@node1 ~]$ mkdir .ssh
[pilou@node1 ~]$ cd .ssh
[pilou@node1 .ssh]$ sudo nano /authorized_keys
[pilou@node1 .ssh]$ exit
logout
Connection to 10.101.1.11 closed.
PS C:\Users\berti> ssh pilou@10.101.1.11
pilou@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Sep 28 16:09:08 2021 from 10.101.1.1
```

## II. Partitionnement

![](https://i.imgur.com/j4YbPoP.png)

```
[pilou@node1 ~]$ lsblk
NAME                        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                           8:0    0    8G  0 disk
├─sda1                        8:1    0    1G  0 part /boot
└─sda2                        8:2    0    7G  0 part
  ├─rl_bastion--ovh1fr-root 253:0    0  6.2G  0 lvm  /
  └─rl_bastion--ovh1fr-swap 253:1    0  820M  0 lvm  [SWAP]
sdb                           8:16   0    3G  0 disk
sdc                           8:32   0    3G  0 disk
sr0                          11:0    1 1024M  0 rom
```

```
[pilou@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for pilou:
  Physical volume "/dev/sdb" successfully created.
[pilou@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```
```
[pilou@node1 ~]$ sudo vgcreate volume /dev/sdb
  Volume group "volume" successfully created
[pilou@node1 ~]$ sudo vgextend volume /dev/sdc
  Volume group "volume" successfully extended
```
```
[pilou@node1 ~]$ sudo lvcreate -L 1G volume -n part1
  Logical volume "part1" created.
[pilou@node1 ~]$ sudo lvcreate -L 1G volume -n part2
  Logical volume "part2" created.
[pilou@node1 ~]$ sudo lvcreate -L 1G volume -n part3
  Logical volume "part3" created.
```
```
[pilou@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: bb37aff5-a440-4e18-a1a1-c252f069ed8c
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```
on repete l'opération

```sudo mkfs -t ext4 /dev/volume/part2```

```sudo mkfs -t ext4 /dev/volume/part3```

```
[pilou@node1 mnt]$ sudo mkdir part1
[pilou@node1 mnt]$ sudo mkdir part2
[pilou@node1 mnt]$ sudo mkdir part3
[pilou@node1 mnt]$ sudo mount /dev/volume/part1 /mnt/part1
[pilou@node1 mnt]$ sudo mount /dev/volume/part2 /mnt/part2
[pilou@node1 mnt]$ sudo mount /dev/volume/part3 /mnt/part3
```

```
[pilou@node1 mnt]$ mount
[...]
/dev/mapper/volume-part1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part3 on /mnt/part3 type ext4 (rw,relatime,seclabel)
```

```sudo nano /etc/fstab```
```
#
# /etc/fstab
# Created by anaconda on Wed Sep 15 08:31:08 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl_bastion--ovh1fr-root /                       xfs     defaults    $
UUID=020f8bb8-f596-45de-9eb2-0853199207ac /boot                   xfs     defaul$
/dev/mapper/rl_bastion--ovh1fr-swap none                    swap    defaults    $
/dev/data/lv_data_1 /mnt/part1 ext4 defaults 0 0
/dev/data/lv_data_2 /mnt/part2 ext4 default 0 0
/dev/data/lv_data_3 /mnt/part3 ext4 default 0 0
```

## III. Gestion de services

## 1. Interaction avec un service existant

## 2. Création de service

### A. Unité simpliste

### B. Modification de l'unité